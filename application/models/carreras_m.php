<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Carreras_m extends CI_Model {
    
    function __contruct() {
        parent:: __contruct();
    }
    
    public function get_all_carrera() {
        $query = $this->db->get('carreras');
        if($query->num_rows() > 0) { // Si hay datos
            return $query->result(); // Devolverlos
        } else { // Si no hay datos
            return false; // Devolver falso
        }
    }
    
    public function quantity_carrera() { // Cantidad de carreras
        $query = $this->db->get('carreras');
        return $query->num_rows();
    }
}