<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios_m extends CI_Model{ // Las funciones del modelo son bastantes claras, por eso no se explica mucho.
    
    function login($Usuario, $Contra){ // La funcion utilizada para comprobar si todos los parametros del usuario son correctos para logear.
        //Tecnica Query Binding.
        $sql = "SELECT * FROM usuarios WHERE usuario = ?"; // Armo la llamada.
        $datos = $this->db->query($sql, array($Usuario)); //La ejecuto.
        foreach($datos->result_array() as $row){
            if($row['usuario'] == $Usuario && $row['contrasena'] == $Contra && $row['tipo']==0 && $row['estado']==1){ //Criterio para determinar el acceso a los administradores.
                echo "Administrador";
                $_SESSION["administrador"] = $row['tipo'];
            }
            if($row['usuario'] == $Usuario && $row['contrasena'] == $Contra && $row['tipo']==1 && $row['estado']==1){ //Criterio para determinar el acceso a los usuarios.
                echo $row['id']; // Si es invitado recibo su ID para tratarla despues, esto me simplifica un par de cosas despues.
                $_SESSION["administrador"] = $row['tipo'];
            }
        }
    }
    /* Se puede observar la validacion, solo deja entrar a los que tengan correcta su combinacion de usuario y contrasena
    y ademas chequea que tipo de usuario son (Administrador o no) y si estan activos.
     */
    /*
    UPDATE usuarios SET contrasena = MD5(contrasena) Esto es si agrego registros manualmente a la base de datos.
    */

    function quantity_alumno_activo() {
        $datos = $this->db->query("SELECT id, usuario from usuarios where tipo = 1 and estado = 1");
        return $datos->num_rows();
    }
    
    function get_alumnos() { // Esta funcion me devuelve los alumnos activos. Se invoca generalmente desde el controlador de cursadas, pero como la funcion es relacionada a usuarios pertenece a este modelo.
        $datos = $this->db->query("SELECT id, usuario from usuarios where tipo = 1 and estado = 1");
        return $datos->result_array();
    }
    
    function get_all_alumno() {
        $datos = $this->db->get('usuarios');
        return $datos->result_array();
    }
    
    function listar() {
        // el ->result_array() es para el formato de los datos
        $datos = $this->db->query("SELECT * FROM usuarios");
        return $datos->result_array();
        
        // otra forma
        //return $this->db->query("SELECT * FROM usuarios")->result_array();
    }
    
    // Funcion para obtener todos los datos de un usuario a partir de un nombre de usuario 
    function datosUsuario($usuario) {
        $datos = $this->db->query("SELECT * FROM usuarios WHERE usuario='{$usuario}'");
        return $datos->result_array();
    }
    
    function add_user($user) {
        $datos = array(
            'usuario'       =>  $user['user_nick'] ,
            'nombre'        =>  $user['user_name'] ,
            'apellido'      =>  $user['user_surname'],
            'tipo'          =>  $user['user_tipo'],
            'estado'        =>  $user['user_estado'],
            'contrasena'    =>  sha1($user['user_password']),
        );
        $this->db->insert('usuarios', $datos); 
    }
    
    function modify_user($user) {
        $datos = array(
            'usuario'       =>  $user['user_nick'] ,
            'nombre'        =>  $user['user_name'] ,
            'apellido'      =>  $user['user_surname'],
            'tipo'          =>  $user['user_tipo'],
            'estado'        =>  $user['user_estado'],
            'contrasena'    =>  sha1($user['user_password']),
        );
        $this->db->update('usuarios', $datos, array('id' => $user['user_id']));
    }
    
    function delete_user($id) {
        $this->db->delete('usuarios', array('id' => $id));
        
    }
    
    function consultar_user($nick) {
        $sql = "SELECT * FROM usuarios WHERE usuario='{$nick}'";
        $datos = $this->db->query($sql);
        return $datos->result_array();
        
    }
    
    function get_usuario_by_id($id) {
        $datos = $this->db->get_where('usuarios', array('id' => $id));
        return $datos->result_array();
        
    }
    
}