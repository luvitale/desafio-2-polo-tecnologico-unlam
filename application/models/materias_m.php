<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Materias_m extends CI_Model { // Las funciones del modelo son bastantes claras, por eso no se explica mucho.
    function get_materia_by_carrera($carrera_id) {
        $query = $this->db->get_where('materias', array('carrera_id' => $carrera_id)); //Consulta CodeIgniter.
        if($query->num_rows() > 0) { // Si hay datos
            return $query->result(); // Devolverlos
        } else { // Si no hay datos
            return false; // Devolver falso
        }
        
        // return $datos->result_array(); //Retorno el resultado de la consulta como un result_array mas facil de manejar.
    }
    
    function get_all_materia( ) { // Esta funcion se necesita en la opcion de agregar cursadas.
        $datos = $this->db->get('materias'); //Consulta CodeIgniter.
        return $datos->result();
    }
    
    function get_all_materia_en_usuario() {
        $datos = $this->db->get('materias'); //Consulta CodeIgniter.
        return $datos->result_array();
    }
    
    function get_materia_by_id($materia_id) { // Esta funcion obtiene los datos de la materia en funcion de su id.
        $datos = $this->db->get_where('materias', array('id' => $materia_id));
        return $datos->result();
    }
    
    function add_materia($carrera_id, $nombre, $descripcion, $carga_horaria) {
        $datos = array(
            'carrera_id' => $carrera_id ,
            'nombre' => $nombre ,
            'descripcion' => $descripcion,
            'carga_horaria' => $carga_horaria,
         );
        $this->db->insert('materias', $datos); 
    }
    
    function delete_materia($id) {
        $this->db->delete('materias', array('id' => $id));
    }
    
    function modify_materia($id, $carrera_id, $nombre, $descripcion, $carga_horaria) {
        $datos = array(
            'carrera_id' => $carrera_id,
            'nombre' => $nombre,
            'descripcion' => $descripcion,
            'carga_horaria' => $carga_horaria,
        );
        $this->db->update('materias', $datos, array('id' => $id));
    }
    
    function consultar_materia_en_carrera($mat_name, $car_id) {
        $sql = "SELECT * FROM materias WHERE carrera_id='{$car_id}' and nombre='{$mat_name}'";
        $datos = $this->db->query($sql);
        return $datos->result_array();
        
    }
}