<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cursadas_m extends CI_Model{ // Las funciones del modelo son bastantes claras, por eso no se explica mucho.

    function get_cursadas($usuario_id){ //Este metodo obtiene las cursadas de un determinado alumno activo.
        //Tecnica Query Binding.
        $sql = "SELECT M.nombre, C.nota, C.fecha, C.id FROM cursadas C LEFT JOIN  materias M ON C.materia_id = M.id WHERE C.usuario_id = ?";
        $datos = $this->db->query($sql, array($usuario_id));
        return $datos->result_array();
    }
    
    function quantity_cursadas_by_usuario($usuario_id) {
        $datos = $this->db->get_where('cursadas', array('usuario_id' => $usuario_id));
        return $datos->num_rows();
    }
    
    function quantity_aprobadas_by_usuario($usuario_id) {
        $sql = "SELECT * FROM cursadas WHERE usuario_id = '{$usuario_id}' and nota >= 4";
        $datos = $this->db->query($sql);
        return $datos->num_rows();
    }
    
    function add($ID_Usuario, $ID_Materia, $Nota){
        $datos = array(
            'usuario_id' => $ID_Usuario ,
            'materia_id' => $ID_Materia ,
            'nota' => $Nota,
        );
        $this->db->insert('cursadas', $datos); 
    }
    
    function delete($id){
         $this->db->delete('cursadas', array('id' => $id));
    }
    
    function cursada($cursada_id){
        //Tecnica Query Binding.
        $sql = "SELECT M.nombre, C.nota, C.fecha, C.id FROM cursadas C LEFT JOIN  materias M ON C.materia_id = M.id WHERE C.id = ?";
        $datos = $this->db->query($sql, array($cursada_id));
        return $datos->result_array();
    }
    
    function get_cursada_by_id($id) {
        $datos = $this->db->get_where('cursadas', array('id' => $id));
        return $datos->result_array();
        
    }
    
    function modify($id, $user, $materia, $nota){
        $datos = array(
            'usuario_id'    =>  $user,
            'materia_id'    =>  $materia,
            'nota'          =>  $nota,
        );
        $this->db->update('cursadas', $datos, array('id' => $id));
    }
    
    function get_all_cursada() {
        $sql = "SELECT U.usuario, M.nombre, C.nota, C.fecha, C.id FROM cursadas C LEFT JOIN  materias M ON C.materia_id = M.id  LEFT JOIN usuarios U ON C.usuario_id = U.id";
        $datos = $this->db->query($sql);
        return $datos->result_array();
    }
    
    
    function enviar_mail($mail, $datos) {
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp-mail.outlook.com',
            'smtp_port' => 587,
            'smtp_user' => 'desafio.mvc@outlook.com',
            'smtp_timeout' => '60',
            'charset' => 'iso-8859-1',
            'smtp_crypto' => 'tls',
            'smtp_pass' => 'desafio1234',
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'newline' => "\r\n"
        );    
 
        $this->email->initialize($config);
        
        $this->email->from('desafio.mvc@outlook.com');
        $this->email->to($mail);
        $this->email->subject("Resultado de sus cursadas");
        $this->email->message($datos);
        $this->email->send();
    }
    
    function enviar_mensaje_alumno($mensaje, $usuario_id){
        $data = array(
            'emisor'    =>  $usuario_id,
            'contenido' =>  $mensaje,
            'receptor'  =>  1, //Por Default el administrador.
            'leido'     =>  0,
        );
        $this->db->insert('mensajes', $data);
    }
    
    function get_mensajes() {
        $sql = "SELECT M.contenido, U.usuario FROM mensajes M LEFT JOIN usuarios U ON U.id = M.receptor WHERE M.leido = 0"; //Armo la consulta.
        $datos = $this->db->query($sql); //Obtengo los mensajes.
        $this->db->set('leido', '1', FALSE);
        $this->db->where('receptor', 1);
        $this->db->update('mensajes');
        return $datos->result_array(); //Retorno el resultado de la consulta como un result_array mas facil de manejar.
    }
    
}