<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller{
    function __construct() {
        parent::__construct();
        $this->load->model('usuarios_m', 'usuarios');
        
    }
    
    // Funcion para acceder a la vista del login
    public function login(){
        if ($this->session->userdata('idUsuario')) {
            redirect('cursada');
        } else {
            $data['titulo'] = 'Iniciar sesión';
            $this->load->view('layout/header', $data);
            $this->load->view("usuarios/login");
            $this->load->view('layout/footer');
        }
    }
    
    
    // Funcion para validar el ingreso de un usuario
    public function validarIngreso(){
        // Una forma mas prolija de obtener los datos de un post en CodeIgniter
        
        if($this->input->post()) {
            $datos = $this->input->post();

            if( empty($datos['usuario']) || empty($datos['contrasena']) ){

                // Si estan bien hechas las validaciones en JS jamas deberia llegar aca!
                $respuesta = "ERROR_INCORRECTO";
                //$mensaje = "Hay datos vacios o incorectos";
                echo $respuesta; 
                return;
            }else{

                // Lo primero que tengo que hacer es saber si existe el usuario ( y si existe traer los datos ) 
                // Necesito una funcion en el model !
                $datosUsuarioRegistrado = $this->usuarios->datosUsuario($datos['usuario']);

                // Si viene vacio, es que no existe el usuario 
                if( empty($datosUsuarioRegistrado) ){

                    $respuesta = "ERROR_NO_EXISTE";
                    //$mensaje = "El usuario no existe";
                    echo $respuesta; 
                    return;
                }
                /*
                echo "<pre>";
                print_r($datosUsuarioRegistrado);
                echo "</pre>";*/

                // Si llegue aca, es porque el usuario es valido, nos falta verificar la contraseña

                if( sha1($datos['contrasena']) == $datosUsuarioRegistrado[0]['contrasena'] ){
                    // Tambien me importa saber el estado


                    if( $datosUsuarioRegistrado[0]['estado'] == 0 ){
                        $respuesta = "ERROR_ESTADO";
                        //$mensaje = "El usuario se encuentra inactivo";
                        echo $respuesta; 
                        return;
                    }


                    // Si llegue aca es porque el usuario es valido, la contraseña es correcta y esta activo

                    // Seteo los datos en session

                    $this->session->set_userdata('idUsuario', $datosUsuarioRegistrado[0]['id']);
                    $this->session->set_userdata('nombre', $datosUsuarioRegistrado[0]['nombre']);
                    $this->session->set_userdata('apellido', $datosUsuarioRegistrado[0]['apellido']);
                    $this->session->set_userdata('usuario', $datosUsuarioRegistrado[0]['usuario']);
                    $this->session->set_userdata('tipo', $datosUsuarioRegistrado[0]['tipo']);
                    $this->session->set_userdata('bienvenida', 1);

                    $respuesta = "OK";
                    //$mensaje = "El usuario es valido";
                    echo $respuesta;
                    return;


                }else{
                    $respuesta = "ERROR_CONTRASENA";
                    //$mensaje = "La contrase&ntilde;a es incorrecta";
                    echo $respuesta; 
                    return;
                }

            }
        } else {
            redirect('usuario');
        }
    }    
    
    // Funcion para desloguearse del sistema 
    public function salir(){
        // destruyo los datos de session
        $this->session->sess_destroy();
        
        // redirecciono al login
        redirect('usuario/login');
    }
    
    
    // funcion para mostrar todos los usuarios registrados
    public function abm(){
        if($this->session->userdata['tipo'] == 0) {
            $data['titulo'] = 'Usuarios';
            $this->load->view('layout/header',$data);
            $this->load->view('usuarios/list_usuario_v');
            $this->load->view('layout/footer');
            $this->load->view('layout/scripts_usuarios');
        } else {
            redirect('cursada');
        }
    }
    
    public function show_user() {
        if($this->session->userdata['tipo'] == 0) {
            
            if($this->input->post('verify') == 'permiso') {
                // Obtengo los datos de la base de datos
                $usuarios = $this->usuarios->listar();

                // Vinculo los datos que voy a enviar
                $data['usuarios'] = $usuarios;

                // Llamo a la vista
                $this->load->view('usuarios/show_user_v', $data);
                
            } else {
                redirect('usuario/abm');
            }
        } else {
            redirect('cursada');
        }
        
    }
    
    public function add_user() {
        if($this->session->userdata['tipo'] == 0) { // Si es administrador y verifica el post
            
            if($this->input->post('verify') == 'permiso') {
                $this->load->view('usuarios/add_user_v');
                
            } else {
                redirect('usuario/abm');
            }
            
        } else {
            redirect('cursada');
        }
    }
    
    public function validar_add_user() {
        if($this->session->userdata['tipo'] == 0) {
            
            if($this->input->post()) {
                $user = $this->input->post();

                if( empty($user['user_nick']) || empty($user['user_password']) || empty($user['user_name']) || empty($user['user_surname'])) {
                    $respuesta = 'ERROR_INCORRECTO';
                    echo $respuesta;
                    return;

                } else {
                    $consulta_user = $this->usuarios->consultar_user($user['user_nick']);
                    if($consulta_user == NULL) {

                        $this->usuarios->add_user($user);

                        $respuesta = 'OK';
                        echo $respuesta;
                        return;

                    } else {
                        $respuesta = 'USER_EXISTS';
                        echo $respuesta;
                        return;

                    }

                }
            } else {
                redirect('usuario/abm');
            }
            
        } else {
            redirect('cursada');
        }
    }
    
    public function modify_user() {
        if($this->session->userdata['tipo'] == 0) { // Si es administrador y verifica el post
            
            if($this->input->post('user_id')) {
                $user_id = $this->input->post('user_id');
                $data['user_modify'] = $this->usuarios->get_usuario_by_id($user_id);
                $this->load->view('usuarios/modify_user_v', $data);
                
            } else {
                redirect('usuario/abm');
            }
            
        } else {
            redirect('cursada');
        }
    }
    
    public function validar_modify_user() {
        if($this->session->userdata['tipo'] == 0) {
            
            if($this->input->post()) {
                $user = $this->input->post();

                if( empty($user['user_id']) || empty($user['user_nick']) || empty($user['user_password']) || empty($user['user_name']) || empty($user['user_surname'])) {
                    $respuesta = 'ERROR_INCORRECTO';
                    echo $respuesta;
                    return;

                } else {
                    $consulta_user = $this->usuarios->consultar_user($user['user_nick']);
                    $array_user = $this->usuarios->get_usuario_by_id($user['user_id']);
                    
                    foreach($array_user as $row_user) {
                        $actual_user = $row_user['usuario']; // Usuario antes de la modificación
                    }
                    
                    if($consulta_user == NULL || $actual_user == $user['user_nick']) { // Si el nombre de usuario está disponible o el que se quiere ingresar es el mismo al que había
                        
                        $this->usuarios->modify_user($user);

                        $respuesta = 'OK';
                        echo $respuesta;
                        return;

                    } else {
                        $respuesta = 'USER_EXISTS';
                        echo $respuesta;
                        return;

                    }

                }
            } else {
                redirect('usuario/abm');
            }
            
        } else {
            redirect('cursada');
        }
    }
    
    public function delete_user() {
        if($this->session->userdata['tipo'] == 0) {
            if($this->input->post()) {
                $user_id = $this->input->post('user_id');
                $this->usuarios->delete_user($user_id);
                return;
                
            } else {
                redirect('usuario');
                
            }
            
        } else {
            redirect('cursada');
        }
    }
        
    public function index(){
        redirect('usuario/login');
    }
        
}

