<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Materia extends CI_Controller{ //Las funciones de esta clase solo estan disponibles para los administradores.
    function __construct() {
        parent::__construct();
        $this->load->model('materias_m', 'materias');
        $this->load->model('carreras_m', 'carreras');
                
        if (!$this->session->userdata('idUsuario')) {
            redirect('usuario');
        } else if($this->session->userdata('tipo') != 0) {
            redirect('cursada');
        }
        
    }
    
    public function index($id){ // La funcion principal.
        /*
        $this->load->view("layout/header");
        $this->load->view("materias/show_materias");
        $this->load->view("layout/footer");
         * 
         */
        
        
        $data['cant_carrera'] = $this->carreras->quantity_carrera();
        
        if( !is_numeric($id) or $id < 0 or $id > $data['cant_carrera'] ) {
            redirect('materia/show_materia/0'); // Redireccionar a todas las carreras
            
            // header("Location: ". $redir);
            
        } else {
            redirect('materia/show_materia/'. $id); // Redireccionar a la carrera seleccionada
            
            // header("Location: ". $redir);
        }
        
    }
    
    public function show_materia($id) {
        $data['cant_carrera'] = $this->carreras->quantity_carrera();
        
        if( !is_numeric($id) or $id < 0 or $id > $data['cant_carrera'] ) {
            redirect('materia/show_materia/0'); // Redireccionar a todas las carreras
            
            // header("Location: ". $redir);
            
        } else {
            $data['id_carrera']=$id; // Almacenar id
            $data['listado_c']=$this->carreras->get_all_carrera(); // Obtener todas las carreras
            $data['titulo']='Materias'; // Definir título
            $this->load->view('layout/header', $data);
            $this->load->view('materias/show_materias_v',$data); // Cargar listado
            $this->load->view('layout/footer');
            $this->load->view('layout/scripts_materias');
            
        }
        
    }
    
    public function add_materia(){ // Funcion que presenta el view para agregar las materias.
        if($_POST['ID_Carrera'] >= 0 && $_POST['verify'] == 'permiso') {  // Si llega ID_Carrera
            $data['listado_c']=$this->carreras->get_all_carrera(); // Obtener todas las carreras
            $carrera_actual = $_POST['ID_Carrera'];
            $data['car_act'] = $carrera_actual;
            $this->load->view('materias/add_materia_v', $data);
        } else {
            redirect('materia');
        }
    }
    
    public function add_llamada(){ // Funcion para confirmar el agregado de materias.
        if($this->input->post()) {  // Si llegan los parámetros 
            $id_carrera = $_POST['i'];
            $nombre_mat = $_POST['n'];
            $descripcion = $_POST['d'];
            $carga_horaria = $_POST['c'];
            $consulta_mat_en_car = $this->materias->consultar_materia_en_carrera($nombre_mat, $id_carrera);
            if($consulta_mat_en_car == NULL) {
                echo "OK";
                $this->materias->add_materia($id_carrera, $nombre_mat, $descripcion, $carga_horaria); // Invoco a la funcion add_materia del modelo que realiza la accion
                return;
                
            } else {
                echo "MATERIA_EXISTS";
                return;
            }

        } else {
            redirect('materia');
        }
    }
    
    public function list_materia($id) { // Funcion que lista las materias.
        if($_POST['ID_Carrera'] >= 0 && $_POST['verify'] == 'permiso') { // Si llegan los parámetros
            $data['cant_carrera'] = $this->carreras->quantity_carrera();

            if( !is_numeric($id) or $id < 0 or $id > $data['cant_carrera'] ) {
                redirect('materia/list_materia/0'); // Redireccionar a todas las carreras

                // header("Location: ". $redir);

            } else {
                $data['id_carrera']=$id; // Almacenar id

                if($id==0) { // Si se selecciona todas las carreras
                    $data['listado_m']=$this->materias->get_all_materia(); // Obtener todas las materias

                } else {
                    $data['listado_m'] = $this->materias->get_materia_by_carrera($id); // Obtener las materias de la carrera seleccionada

                }
                $data['listado_c']=$this->carreras->get_all_carrera(); // Obtener todas las carreras
                $this->load->view('materias/list_materia_v',$data); // Cargar listado

            }
        } else {
            redirect('materia');
        }
        
        /*
        $carrera_id = $_POST['ID_Carrera']; // El parametro que viene de la vista.
         $redir = base_url() . "materia/show_materia" . $carrera_id;
         header("Location: ". $redir);
         $materias=$this->materias->listar_materia_by_carrera($carrera_id); // Invoco a la funcion listar del modelo y almaceno el resultado en la variable materias.
         $data['materias']=$materias; // Defino que argumentos voy a pasar a la vista.
         $this->load->view('materias/list_materia_v.php', $data); // Voy a la vista.s
        */
    }
    
    public function delete_materia(){ // Funcion que elimina las materias.
        if($_POST['ID_Materia']) { // Si llega ID_Materia
            $ID_Materia = $_POST['ID_Materia']; // El parametro que viene de la vista.
            $this->materias->delete_materia($ID_Materia); // Invoco a la funcion eliminar del modelo y no devuelve nada obviamente, solo lo elimina.
        } else {
            redirect('materia');
        }
    }
    
    public function modify_materia(){ // Funcion que modifica las materias.
        if($_POST['ID_Materia']) { // Si llega ID_Materia
            $ID_Materia = $_POST['ID_Materia']; // El parametro que viene de la vista.
            $materias=$this->materias->get_materia_by_id($ID_Materia); // Invoco a la funcion materia que me devuelve los datos de la materia, esto sirve para que la viste donde modifico la materia tenga los datos.
            $data['carreras']=$this->carreras->get_all_carrera();
            $data['mat_modify']=$materias;  // Defino que argumentos voy a pasar a la vista.
            $this->load->view('materias/modify_materia_v', $data); //Voy a la vista donde se editan los parametros de la materia.
        } else {
            redirect('materia');
        }
    }
    
    public function modify_llamada(){ // Funcion que modifica las materias.
        if($this->input->post()) { // Si llegan los parámetros
            // Recibo todos los parametros.
            $id_materia = $_POST['id'];
            $id_carrera = $_POST['i'];
            $nombre_mat = $_POST['n'];
            $descripcion = $_POST['d'];
            $carga_horaria = $_POST['c'];
            
            $consulta_mat_en_car = $this->materias->consultar_materia_en_carrera($nombre_mat, $id_carrera);
            $array_materia = $this->materias->get_materia_by_id($id_materia);
            
            foreach($array_materia as $row_materia) { // Cargo los datos de la materia antes de la modificación
                $actual_nombre_mat = $row_materia->nombre;
                $actual_carrera_id = $row_materia->carrera_id;
            }
            
            if( $consulta_mat_en_car == NULL || ( $id_carrera == $actual_carrera_id && $nombre_mat == $actual_nombre_mat ) ) { // Si no existe la materia o se ingresa como nombre de la materia la que se está usando
                
                $this->materias->modify_materia($id_materia, $id_carrera, $nombre_mat, $descripcion, $carga_horaria); // Invoco a la funcion modify_materia del modelo que realiza la accion
                echo "OK";
                return;
                
            } else {
                echo "MATERIA_EXISTS";
                return;
            }
            
        } else {
            redirect('materia');
        }
    }
    
}
