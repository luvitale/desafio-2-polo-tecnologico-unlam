<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//session_start();     // Con esto habilito el archivo "Cursadac.php" para acceder a variables de sesión

class Cursada extends CI_Controller{
    function __construct() {
        parent::__construct();
        $this->load->model('cursadas_m', 'cursadas');
        $this->load->model('usuarios_m', 'usuarios'); //Tambien requiero a usuarios.
        $this->load->model('materias_m', 'materias'); //Tambien requiero a materias.
        
        if (!$this->session->userdata('idUsuario')) { // Si no hay una sesión abierta
            redirect('usuario'); // Redireccionar al controlador usuario
        }
    }
    
    /* index invitado anterior
    public function index(){ // El index esta disponible para los Usuarios(Alumnos) que desean ver el historial de sus materias.
        echo $this->session->userdata('nombre');
        echo $this->session->userdata('tipo');
        $Usuario= $this->session->userdata('idUsuario'); //Obtengo los parametros de la llamada a traves Ajax.
        $cursadas=$this->cursadas->get_cursadas($Usuario); // Invoco a la funcion listar del modelo y almaceno el resultado en la variable materias.
        $data['cursadas']=$cursadas; // Defino que argumentos voy a pasar a la vista.
        $this->load->view('cursadas/show_cursada_v', $data); // Voy a la vista.s
    }
     *
     */
    
    public function index() {
        if($this->session->userdata('tipo') == 0) { // Si es administrador
            $usuarios= $this->usuarios->get_alumnos(); // Obtengo todos los alumnos activos.
            $mensajes = $this->cursadas->get_mensajes(); // Obtengo todos los mensajes.
            $nombre = $this->session->userdata('nombre');
            $apellido = $this->session->userdata('apellido');
            $data['mensajes'] = $mensajes; // Los preparo para llevar a la vista.
            $data['usuarios'] = $usuarios; // Los preparo para llevar a la vista.
            $data['titulo'] = 'Cursadas';
            $data['persona'] = $nombre . " " . $apellido;
            $this->load->view('layout/header', $data);
            $this->load->view('cursadas/cursadas_admin_v', $data); // Voy a la vista y le paso los alumnos.
            $this->load->view('layout/footer');
            $this->load->view('layout/scripts_cursadas');
            $bienvenida = $this->session->userdata('bienvenida');
            if($bienvenida == 1) {
                $this->load->view('extras/bienvenida_v', $data);
                $this->session->set_userdata('bienvenida', 0);
            }
            
        } else { // Si es invitado
            $Usuario= $this->session->userdata('idUsuario'); //Obtengo los parametros de la llamada a traves Ajax.
            $cursadas=$this->cursadas->get_cursadas($Usuario); // Invoco a la funcion listar del modelo y almaceno el resultado en la variable materias.
            $nombre = $this->session->userdata('nombre');
            $apellido = $this->session->userdata('apellido');
            $data['usuario_id_enviar'] = $Usuario;
            $data['cursadas']=$cursadas; // Defino que argumentos voy a pasar a la vista.
            $data['titulo'] = "Cursadas de " . $nombre . " " . $apellido;
            $data['persona'] = $nombre . " " . $apellido;
            $this->load->view('layout/header', $data);
            $this->load->view('cursadas/show_cursada_v', $data); // Voy a la vista
            $this->load->view('extras/enviar_mensaje_v');
            $this->load->view('layout/footer');
            $this->load->view('layout/scripts_alumno');
            $bienvenida = $this->session->userdata('bienvenida');
            if($bienvenida == 1) {
                $this->load->view('extras/bienvenida_v', $data);
                $this->session->set_userdata('bienvenida', 0);
            }
        }
        
    }
    
    /* index admin anterior
    public function index_admin(){ // Cuando un administrador accede al ABM de cursadas.
        $usuarios= $this->usuarios->get_alumnos(); // Obtengo todos los alumnos activos.
        $data['usuarios'] = $usuarios; // Los preparo para llevar a la vista.
        $data['titulo'] = 'Cursada';
        $this->load->view('layout/header', $data);
        $this->load->view('cursadas/cursadas_admin_v', $data); // Voy a la vista y le paso los alumnos.
        $this->load->view('layout/footer');
    
    }
     * 
     */
    
    public function perfil() {
        if($this->session->userdata('tipo') == 1) { // Si es usuario
            $Usuario = $this->session->userdata('idUsuario'); //Obtengo los parametros de la llamada a traves Ajax.
            $user_nick = $this->session->userdata('usuario');
            $nombre = $this->session->userdata('nombre');
            $apellido = $this->session->userdata('apellido');
            $cant_materias_cursadas = $this->cursadas->quantity_cursadas_by_usuario($Usuario);
            $cant_materias_aprobadas = $this->cursadas->quantity_aprobadas_by_usuario($Usuario);
            $data['titulo'] = "Perfil de " . $nombre . " " . $apellido;
            $data['nombre'] = $nombre;
            $data['apellido'] = $apellido;
            $data['user_nick'] = $user_nick;
            $data['cant_mat_cursadas'] = $cant_materias_cursadas;
            $data['cant_mat_aprob'] = $cant_materias_aprobadas;
            $this->load->view('layout/header', $data);
            $this->load->view('cursadas/perfil_v', $data);
            $this->load->view('layout/footer');
            
        } else {
            redirect('cursada');
        }
    }
    
    public function get_all_cursada() { // Obtener todas las cursadas
        if($this->session->userdata('tipo') == 0 && $_POST['verify'] == 'permiso') { // Si es administrador
            $cursadas = $this->cursadas->get_all_cursada();
            $data['cursadas'] = $cursadas;
            $this->load->view('cursadas/list_all_admin_v', $data);
        } else {
            redirect('cursada');
        }
    }
    
    public function get_cursada_by_usuario(){ // Esta funcion es llamada desde la vista, donde el usuario ya esta validado de que esta activo.
        if($this->session->userdata('tipo') == 0 && $_POST['Usuario']) { // Si es administrador y llega Usuario
            $Usuario= $_POST['Usuario']; //Obtengo los parametros de la llamada a traves Ajax.
            $cursadas=$this->cursadas->get_cursadas($Usuario); // Invoco a la funcion listar del modelo y almaceno el resultado en la variable materias.
            $data['cursadas']=$cursadas; // Defino que argumentos voy a pasar a la vista.
            $this->load->view('cursadas/list_admin_v', $data); // Voy a la vista
        } else {
            redirect('cursada'); // Redireccionar al controlador cursada
        }
    }
    
    public function delete_cursada(){ // Esta funcion elimina la cursada.
        if($this->session->userdata('tipo') == 0 && $_POST['ID_Cursada'] > 0) { // Si es administrador y llega ID_Cursada
            $ID_Cursada = $_POST['ID_Cursada']; // El parametro que viene de la vista.
            $this->cursadas->delete($ID_Cursada); // Invoco a la funcion eliminar del modelo y no devuelve nada obviamente, solo lo elimina.
        } else {
            redirect('cursada'); // Redireccionar al controlador cursada
        }
    }
    
    public function modify_cursada(){ // Funcion que modifica las cursadas, paso1.
        if($this->session->userdata('tipo') == 0 && $_POST['Alumno'] && $_POST['ID_Cursada'] > 0) { // Si es administrador y llega Alumno y ID_Cursada
            
            $usuarios= $this->usuarios->get_alumnos();
            $materias= $this->materias->get_all_materia_en_usuario();
            $data['materias'] = $materias;
            $data['usuarios'] = $usuarios;
            $ID_Cursada = $_POST['ID_Cursada']; // El parametro que viene de la vista.
            $Alumno = $_POST['Alumno'];
            $cursadas = $this->cursadas->get_cursada_by_id($ID_Cursada); // Invoco a la funcion cursada que me devuelve los datos de la cursada, esto sirve para que la viste donde modifico la cursada tenga los datos.
            $data['cursadas'] = $cursadas;  // Defino que argumentos voy a pasar a la vista.
            // print_r($cursadas);
            $data['alumno'] = $Alumno;
            $this->load->view('cursadas/modify_cursada_v', $data); //Voy a la vista donde se editan los parametros de la cursadas.
        } else {
            redirect('cursada');
        }
    }
    
    public function modify_llamada(){ // Funcion que modifica las cursadas, paso2.
        
        if($this->session->userdata('tipo') == 0 && $this->input->post()) { // Si es administrador y recibo datos
            $id_cursada = $_POST['id_cursada'];
            $user = $_POST['cur_user'];
            $materia = $_POST['cur_mat'];
            $nota = $_POST['cur_nota'];
            $this->cursadas->modify($id_cursada, $user, $materia, $nota); // Invoco a la funcion modify del modelo que realiza la accion
            
        } else {
            redirect('cursada');
        }
    }
    
    public function add_cursada(){ // Funcion que presenta el view para agregar las cursadas.
        if($this->session->userdata('tipo') == 0 && $_POST['user_act'] >= 0 && $_POST['verify'] == 'permiso') { // Si es administrador
            $usuarios= $this->usuarios->get_alumnos();
            $materias= $this->materias->get_all_materia_en_usuario();
            $data['user_act'] = $_POST['user_act'];
            $data['materias'] = $materias;
            $data['usuarios'] = $usuarios;
            $this->load->view('cursadas/add_cursada_v', $data);
        } else {
            redirect('cursada');
        }
    }
    public function add_llamada(){ // Funcion que presenta el view para agregar las cursadas.
        if($this->session->userdata('tipo') == 0 && $_POST['i'] > 0 && $_POST['m'] > 0 && $_POST['n'] >= 0) { // Si es administrador y se recibe información de $_POST['i'], $_POST['m'], $_POST['n']
            $ID_Usuario = $_POST['i'];
            $ID_Materia = $_POST['m'];
            $nota = $_POST['n'];
            $this->cursadas->add($ID_Usuario, $ID_Materia, $nota); // Invoco a la funcion add del modelo que realiza la accion
        } else {
            redirect('cursada');
        }
    }
    
    
    public function enviar_mail() {
        if($this->session->userdata('tipo')==1) {
            if($this->input->post()) {
                $mail = $this->input->post('mail');
                $datos = $this->input->post('datos');
                if(empty($mail)) {
                    echo "EMPTY";
                } else {
                    echo "OK";
                    $this->cursadas->enviar_mail($mail, $datos);
                }
                
                
            } else {
                redirect('cursada');
            }
        } else {
            redirect('cursada');
        }
    }
    
    public function enviar_mensaje_alumno() {
        if($this->session->userdata('tipo')==1) {
            if($this->input->post()) {
                $mensaje = $this->input->post('mensaje');
                $usuario_id = $this->input->post('usuario_id');
                if(empty($mensaje)) {
                    echo "EMPTY_MSG";
                    return;
                    
                } else if(empty($usuario_id)) {
                    echo "EMPTY_USER";
                    return;
                    
                } else {
                    echo "OK";
                    $this->cursadas->enviar_mensaje_alumno($mensaje, $usuario_id);
                }
                
            } else {
                redirect('cursada');
            }
        } else {
            redirect('cursada');
        }
    }
    
}