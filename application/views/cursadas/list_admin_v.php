<table class='table table-striped table-hover table-bordered' id="myTableListCursada"> 
    <thead class="thead_admin_list_cur">
        <tr>
            <th>Materia</th>
            <th>Nota</th>
            <th>Fecha</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tfoot class="tfoot_admin_list_cur">
        <tr>
            <th>Materia</th>
            <th>Nota</th>
            <th>Fecha</th>
            <th>Opciones</th>
        </tr>
    </tfoot>
    <tbody class="tbody_admin_list_cur">
        <?php foreach ($cursadas as $row) : ?>
            <tr>
                <td><?php echo $row['nombre'] ?></td>
                <td><?php echo $row['nota'] ?></td>
                <td><?php echo $row['fecha'] ?></td>
                <td>
                    <button type="submit" class="btn btn-danger delete" data-toggle="modal" data-target="#myModalDelCur" id="<?php echo $row['id'] ?>"onClick="delete_cursada(this.id);"> Eliminar </button>
                    <button type="submit" class="btn btn-warning modify" id="<?php echo $row['id'] ?>" onClick="modify_cursada(this.id);"> Modificar </button>
                </td>
            </tr>

        <?php endforeach; ?>
    </tbody>
</table>

<?php $this->load->view('extras/modal_del_cur'); ?>