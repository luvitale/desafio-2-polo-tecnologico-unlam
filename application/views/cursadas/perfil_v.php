<div class="container">
    
    <header class="col-ms-12">
        <button class="btn btn-info" id="goCursadaUser" onclick="go_cursada_user();">Ir a Cursadas</button>
        <a class='btn btn-warning' id='close_ses_user' href="<?php echo base_url()."usuario/salir"; ?>">Cerrar Sesión</a>
    </header>
    
    <section id="datos_usuario">
        <h1><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Perfil</h1>
        <article class="col-ms-12" id="datos_usuario_identidad">
            <h2>Usuario: <?php echo $user_nick ?></h2>
            <hr>
            <h2>Nombre: <?php echo $nombre ?></h2>
            <h2>Apellido: <?php echo $apellido ?></h2>
            <hr>
        </article>
        <article class="col-ms-12" id="datos_usuario_materias">
            <h2>Materias cursadas: <?php echo $cant_mat_cursadas ?></h2>
            <h2>Materias aprobadas: <?php echo $cant_mat_aprob ?></h2>
        </article>
    </section> 
    
</div>