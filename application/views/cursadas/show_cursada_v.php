<div class='container'>
    
    <header>
        <button class="btn btn-info" id="goPerfil" onclick="go_perfil();">Ir a Perfil</button>
        <a class='btn btn-warning' id='close_ses_user' href="<?php echo base_url()."usuario/salir"; ?>">Cerrar Sesión</a>
        <h1 id='h1_user'>Lista de materias cursadas</h1>
    </header>
    
    <div class='col-ms-12'>
        
        <table class='table table-striped table-hover table-bordered' id='myTableListUserCursada'> 
            <thead class='thead_user'>
                <tr>
                    <th class='th_user'>Materia</th>
                    <th class='th_user'>Nota</th>
                    <th class='th_user'>Fecha</th>
                </tr>
            </thead>

            <tfoot class='tfoot_user'>
                <tr>
                    <th class='th_user'>Materia</th>
                    <th class='th_user'>Nota</th>
                    <th class='th_user'>Fecha</th>
                </tr>
            </tfoot>

            <tbody class='tbody_user'>
                <?php $nota_mat_i = 0 ?>
                <?php foreach ($cursadas as $row): ?>
                    <tr>
                        <td class='td_user' id="nombre_mat_alum"><?php echo $row['nombre'] ?></td>
                        <td class='td_user' id="nota_mat_alum<?php echo $nota_mat_i ?>"><?php echo $row['nota'] ?></td>
                        <td class='td_user' id="fecha_mat_alum"><?php echo $row['fecha'] ?></td>
                    </tr>
                    <?php $nota_mat_i++ ?>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>

</div>