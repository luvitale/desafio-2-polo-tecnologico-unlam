<table class='table table-striped table-hover table-bordered'>
    <thead class="thead_admin_mod_cur">
        <tr>
            <th>Usuario</th>
            <th>Materia</th>
            <th>Nota</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <!--
    <tfoot class="tfoot_admin_mod_cur">
        <tr>
            <th>Usuario</th>
            <th>Materia</th>
            <th>Nota</th>
            <th>Fecha</th>
            <th>Opciones</th>
        </tr>
    </tfoot>
    -->
    <tbody class="tbody_admin_mod_cur">
        <?php foreach ($cursadas as $row) : ?>
            <tr>
                <td>
                    <select name="cur_user_select" class="form-control" id="cur_user_select">
                        <?php foreach($usuarios as $user_cur) : ?>
                            <option class="usuarios" value="<?php echo $user_cur['id'] ?>" <?php if($user_cur['id'] == $row['usuario_id']) : ?>selected="selected"<?php endif; ?>><?php echo $user_cur['usuario'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>

                <td>
                    <select name="mat_user_select" class="form-control" id="mat_user_select"/>
                    <?php foreach($materias as $mat_cur) : ?>
                        <option class="usuarios" value="<?php echo $mat_cur['id'] ?>" <?php if($mat_cur['id'] == $row['materia_id']) : ?>selected="selected"<?php endif; ?>><?php echo $mat_cur['nombre'] ?></option>
                    <?php endforeach; ?>
                    </select>
                </td>

                <td>
                    <input type="number" min="0" max="10" class="form-control" id="nota_user" name="nota_user" placeholder="<?php echo $row['nota'] ?>" onKeyPress="return validar_numeros(event);">
                </td>

                <td>
                    <button type="submit" class="btn btn-warning modify" id="<?php echo $row['id'] ?>" onClick="aceptar_cursada(this.id);"> Modificar </button>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<button id="volver_a_cursadas" class="btn btn-success" onclick="show_usuarios();">&leftarrow; Volver</button>