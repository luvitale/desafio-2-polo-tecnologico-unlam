<div class="container">
    <header class="col-sm-12">
        
        <button type="submit" class="btn btn-info" id="goMaterias" onClick="go_materias();">Ir a Materias</button>
        <button type="submit" class="btn btn-info" id="goMaterias" onClick="go_usuarios();">Ir a Usuarios</button>
        <a class='btn btn-warning' id="close_ses_user" href="<?php echo base_url()."usuario/salir"; ?>">Cerrar Sesión</a>
        <form>
            <div class="form-group">
                <!-- Seleccionador de alumnos para mostrar información de acuerdo a la opción elegida -->
                <label for="usuarios" id="txtUsuarios">Filtrar por alumno:</label>
                <select name="usuarios" id="usuarios" onchange="show_usuarios(this.value);">
                    <option class="carrera" value="0">Todos</option>
                    <?php foreach($usuarios as $row): ?>
                        <option class="usuarios" value="<?php echo $row['id'] ?>"><?php echo $row['usuario'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </form>

        <!-- Botón para agregar cursadas -->
        <button type="submit" id="add_cursada" class="btn btn-success" onClick="add_cursada();">Agregar Cursada</button>

    </header>
    
    <!-- Tabla que se modifica con la interacción -->
    <div id="txtHintCur" class="col-sm-12 table-responsive"></div>
        
</div>

<?php //Con esta funcion imprimo los mensajes de los usuarios.
foreach($mensajes as $row): ?>
    <script type="text/javascript"> 
        alert("Alumno: <?php echo $row['usuario'] ?>\n\Mensaje: <?php echo $row['contenido'] ?>");
    </script>
<?php endforeach; ?>