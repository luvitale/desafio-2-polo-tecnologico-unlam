<table class='table table-striped table-hover table-bordered'>
    <thead class="thead_admin_add_cur">
        <tr>
            <th>Usuario</th>
            <th>Materia</th>
            <th>Nota</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody class="tbody_admin_add_cur">
        <td>
            <select name="id_Usuario" class="form-control" id="id_Usuario">
                <?php foreach($usuarios as $row) : ?>
                    <option class="usuarios" value="<?php echo $row['id'] ?>" <?php if($user_act != 0 && $user_act == $row['id']) : ?>selected="selected"<?php endif; ?>><?php echo $row['usuario'] ?></option>
                <?php endforeach; ?>
            </select>
        </td>

        <td>
            <select name="id_Materia" class="form-control" id="id_Materia"/>
                <?php foreach($materias as $row) : ?>
                    <option class="usuarios" value="<?php echo $row['id'] ?>"><?php echo $row['nombre'] ?></option>
                <?php endforeach; ?>
            </select>
        </td>

        <td>
            <input type="number" min="0" max="10" class="form-control" id="nota" name="nota" onKeyPress="return validar_numeros(event);">
        </td>

        <td>
            <button type="submit" class="btn btn-info add" id="agregar_mate" onClick="add_cursada_llamada(id_Usuario.value,id_Materia.value,nota.value);"> Agregar </button>
        </td>

    </tbody>

</table>

<button id="volver_a_cursadas" class="btn btn-success" onclick="show_usuarios();">&leftarrow; Volver</button>