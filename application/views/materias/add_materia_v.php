<table class='table table-striped table-hover table-bordered w3-table-all'>
    <thead class="thead_addmat">
        <tr>
            <th>Carrera</th>
            <th>Materia</th>
            <th>Descripcion</th>
            <th>Carga Horaria</th>
            <th>Confirmación</th>
        </tr>
    </thead>
    <tbody class="tbody_addmat">
        <tr>
            <td id="td__addmat">
                <select id="id_Carrera" class="form-control">
                    <?php foreach($listado_c as $carrera): ?>
                        <option value="<?php echo $carrera->id ?>" <?php if($car_act != 0 && $carrera->id == $car_act): ?>selected='selected'<?php endif; ?>><?php echo $carrera->nombre ?></option>
                    <?php endforeach; ?>
                </select>
            </td>
            <td id="td__addmat">
                <input type="text" class="form-control" id="nombre" name="nombre" onKeyPress="return validar_letras_materia(event);">
            </td>
            <td id="td__addmat">
                <input type="text" class="form-control" id="descripcion" name="descripcion" onKeyPress="return validar_letras(event);">
            </td>
            <td id="td__addmat">
                <select name="carga_horaria" class="form-control" id="carga_horaria">
                    <option value="2">2</option>
                    <option value="4">4</option>
                    <option value="6">6</option>
                    <option value="8">8</option>
                    <option value="10">10</option>
                </select>
            </td>
            <td id="td__addmat">
                <button type="submit" class="btn btn-primary" id="agregar_mate" onClick="add_materia(id_Carrera.value, nombre.value,descripcion.value, carga_horaria.value, event);"> Agregar </button>
            </td>
        </tr>
    </tbody>
</table>
<button id="volver_a_materia" class="btn btn-success" onClick="show_materia_by_id(<?php echo $car_act ?>)">&LeftArrow; Volver</button>