<table class="table table-striped table-hover table-bordered" id="myTableListMateria">
    <thead class="tabla-head-show" id="myTableHead">
        <tr>
            <?php if($id_carrera == 0) : ?>
            <th id="th__show">Carrera</th>
            <?php endif ?>
            <th id="th__show">Materia</th>
            <th id="th__show">Descripción</th>
            <th id="th__show">Carga Horaria</th>
            <th id="th__show">Opciones</th>
        </tr>
    </thead>
    <tfoot class="tabla-head-show" id="myTableFoot">
        <tr>
            <?php if($id_carrera == 0) : ?>
            <th id="th__show">Carrera</th>
            <?php endif ?>
            <th id="th__show">Materia</th>
            <th id="th__show">Descripción</th>
            <th id="th__show">Carga Horaria</th>
            <th id="th__show">Opciones</th>
        </tr>
    </tfoot>
    <tbody class="tabla-body-show" id="myTableBody">
        <?php foreach($listado_m as $materia): ?>
        <tr>
            <?php if($id_carrera == 0) : ?>
            <td id="td__show"><?php echo $listado_c[$materia->carrera_id - 1]->nombre ?></td>
            <?php endif ?>
            <td id="td__show"><?php echo $materia->nombre ?></td>
            <td id="td__show"><?php echo $materia->descripcion ?></td>
            <td id="td__show"><?php echo $materia->carga_horaria ?></td>
            <td id="td__show">
                <button id="<?php echo $materia->id ?>" class="btn btn-info modify" onclick="modify_materia(this.id, event);">Modificar</button>
                <button id="<?php echo $materia->id ?>" class="btn btn-danger delete" onclick="delete_materia(this.id, <?php echo $materia->carrera_id ?>, event);" data-toggle="modal" data-target="#myModalDelMat">Eliminar</button>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php $this->load->view('extras/modal_del_mat'); ?>