<div class="container">
    
    <header class="col-sm-12">
        
        <button type="submit" class="btn btn-info" id="goCursadas" onClick="go_cursadas();">Ir a Cursadas</button>
        <button type="submit" class="btn btn-info" id="goMaterias" onClick="go_usuarios();">Ir a Usuarios</button>
        <a class='btn btn-warning' id="close_ses_user" href="<?php echo base_url()."usuario/salir"; ?>">Cerrar Sesión</a>
        
        <div class="form-group">
            
            <!-- Seleccionador de carrera para mostrar información de acuerdo a la opción elegida -->
            <label for="filtroCarrera">Filtrar por carrera:</label>
            <select id="filtroCarrera">
                <?php if($id_carrera == 0) : ?>
                    <option value="0" selected="selected">Todas</option>
                <?php else : ?>
                    <option value="0">Todas</option>
                <?php endif ?>
                    <?php foreach($listado_c as $carrera): ?>
                        <?php if($id_carrera == $carrera->id): ?>
                            <option value="<?php echo $carrera->id ?>" selected="selected"><?php echo $carrera->nombre ?></option>
                        <?php else: ?>
                            <option value="<?php echo $carrera->id ?>"><?php echo $carrera->nombre ?></option>
                        <?php endif ?>
                    <?php endforeach; ?>
            </select>
            
        </div>
            
        <!-- Botón para agregar materias -->
        <button type="submit" id="agregar_tabla" class="btn btn-success">Agregar Materia</button>
            
    </header>
            
    <!-- Tabla que se modifica con la interacción -->
    <div id="txtHintMat" class="col-sm-12 table-responsive"></div>

</div>