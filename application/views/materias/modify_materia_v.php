<table class='table table-striped table-hover table-bordered'>
    <thead class="thead_modmat">
        <tr>
            <th>Carrera</th>
            <th>Materia</th>
            <th>Descripcion</th>
            <th>Carga Horaria</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody class="tbody_modmat">
        <tr>
            <?php foreach($mat_modify as $matinfo) : ?>
            <td id="td__modmat">
                <select name="id_Carrera" class="form-control" id="id_Carrera">
                <?php foreach($carreras as $car) : ?>
                    <option value="<?php echo $car->id ?>"<?php if($car->id == $matinfo->carrera_id): ?>selected="selected"<?php endif; ?>><?php echo $car->nombre ?></option>
                <?php endforeach; ?>
                </select>
            </td>
            <td id="td__modmat">
                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="<?php echo $matinfo->nombre ?>" onKeyPress="return validar_letras_materia(event);">
            </td>
            <td id="td__modmat">
                <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="<?php echo $matinfo->descripcion ?>" onKeyPress="return validar_letras(event);">
            </td>
            <td id="td__modmat">
                <select name="carga_horaria" class="form-control" id="carga_horaria" name="carga_horaria">
                    <?php for($i=2;$i<=10;$i+=2): ?>
                    <option value="<?php echo $i ?>" <?php if($i == $matinfo->carga_horaria): ?>selected="selected"<?php endif; ?>><?php echo $i ?></option>
                    <?php endfor; ?>
                </select>
            </td>
            <td id="td__modmat">
                <button type="submit" id="<?php echo $matinfo->id ?>" onClick="aceptar_modify_materia(this.id, id_Carrera.value, nombre.value,descripcion.value, carga_horaria.value, event);" class="btn btn-warning"> Modificar </button>
            </td>
            <?php endforeach; ?>
        </tr>
    </tbody>
</table>

<button id="volver_a_materia" class="btn btn-success" onClick="show_materia_by_id(<?php echo $matinfo->carrera_id ?>)">&LeftArrow; Volver</button>