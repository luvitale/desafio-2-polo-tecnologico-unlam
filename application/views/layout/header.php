<!doctype html>
<html>
   
    <head>
       
        <title><?php echo $titulo ?></title>
        
        <!-- Codificación de caracteres -->
        <meta charset="utf-8">
        
        <!-- Ícono de la pestaña -->
        <link rel="icon" type="image/x-icon" href="<?php echo base_url() ?>assets/img/favicon.ico">
        
        <!-- Necesario para hacer responsive -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <!-- Estilos propios -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/estilos.css">
        
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/vendor/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/vendor/bootstrap-theme.min.css">
        
        <!-- DataTables -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/vendor/dataTables.min.css">
    </head>
    <body>