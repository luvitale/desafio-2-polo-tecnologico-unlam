        
        <!-- Modernizr -->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/vendor/modernizr.min.js"></script>
        
        <!-- JQuery -->
        <script>window.jQuery || document.write('<script type="text/javascript" src="<?php echo base_url() ?>assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/vendor/redirect.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/vendor/dataTables.min.js"></script>
        
        <!-- Bootstrap -->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/vendor/bootstrap.min.js"></script>
        
        <!-- Scripts propios -->
        <script type="text/javascript">
        // Pasar variables de PHP a Javascript
        var base_url = '<?php echo base_url() ?>';
        
        // Definir decisión
        var decision;
        
        // Definir caracteres para validación
        var null_caracter = 0;
        var num_cero = 48;
        var num_nueve = 57;
        var space = 32;
        var interrog_cierre = 63;
        var a_mayus = 65;
        var z_mayus = 90;
        var a_minus = 97;
        var z_minus = 122;
        var interrog_apertura = 191;
        var u_con_dieresis = 252;
        var comilla = 39;
        var punto = 46;
        var guion_bajo = 95;
        </script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/login.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/go_redirection.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/validate.js"></script>
        
    </body>

</html>