<div class="modal fade" id="myModalDeBienvenida">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Bienvenid@ <?php echo $persona ?></h4>
      </div>
      <div class="modal-body">
        <p>Ha ingresado al sistema</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script type="text/javascript">
    $(document).ready(function(){
        $('#myModalDeBienvenida').modal('toggle');
    });
</script>