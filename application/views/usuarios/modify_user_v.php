<div class="col-ms-12 table-responsive">
    <table class='table table-striped table-hover table-bordered' id='myTableUsers'> 
        <thead class='thead_user_modify'>
            <tr>
                <th class='th_user_modify'>Usuario</th>
                <th class='th_user_modify'>Contraseña</th>
                <th class='th_user_modify'>Nombre</th>
                <th class='th_user_modify'>Apellido</th>
                <th class='th_user_modify'>Tipo</th>
                <th class='th_user_modify'>Estado</th>
                <th class='th_user_modify'>Opciones</th>
            </tr>
        </thead>

        <tbody class='tbody_user_modify'>
            <?php foreach($user_modify as $row): ?>
                <tr>
                    <td class='td_user_modify'>
                        <input type="text" placeholder="<?php echo $row['usuario'] ?>" autofocus="autofocus" required id="user_nick" name="user_nick" class="form-control" onkeypress="return validar_user_nick(event);">
                    </td>
                    <td class='td_user_modify'>
                        <input type="password" placeholder="Contraseña" required id="user_password" name="user_password" class="form-control">
                    </td>
                    <td class='td_user_modify'>
                        <input type="text" placeholder="<?php echo $row['nombre'] ?>" required id="user_name" name="user_name" class="form-control" onkeypress="return validar_letras(event);">
                    </td>
                    <td class='td_user_modify'>
                        <input type="text" placeholder="<?php echo $row['apellido'] ?>" required id="user_surname" name="user_surname" class="form-control" onkeypress="return validar_letras(event);">
                    </td>
                    <td class='td_user_modify'>
                        <select id="user_tipo_select" name="user_tipo_select" class="form-control">
                            <option id="0" value="0" <?php if($row['tipo'] == 0): ?>selected="selected"<?php endif; ?>>Administrador</option>
                            <option id="1" value="1" <?php if($row['tipo'] == 1): ?>selected="selected"<?php endif; ?>>Usuario</option>
                        </select>
                    </td>
                    <td class='td_user_modify'>
                        <select id="user_estado_select" name="user_estado_select" class="form-control">
                            <option id="0" value="0" <?php if($row['estado'] == 0): ?>selected="selected"<?php endif; ?>>Inactivo</option>
                            <option id="1" value="1" <?php if($row['estado'] == 1): ?>selected="selected"<?php endif; ?>>Activo</option>
                        </select>
                    </td>
                    <td class='td_user_modify'>
                        <button class="btn btn-warning" id="<?php echo $row['id'] ?>" name="modify_user_button" onclick="modify_user_llamada(this.id);">Modificar</button>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<button class="btn btn-success" id="volver_a_usuarios" onclick="volver_a_usuarios();">&leftarrow; Volver</button>
<br>

<div class="col-md-4 col-md-offset-4" id="informacion" hidden>
    <div class="alert alert-danger" role="alert">
        <div id="informacion-texto"></div>
    </div>
</div>