<div class="col-ms-12">
    <table class='table table-striped table-hover table-bordered' id='myTableUsers'> 
        <thead class='thead_user_list'>
            <tr>
                <th class='th_user_list'>Usuario</th>
                <th class='th_user_list'>Nombre</th>
                <th class='th_user_list'>Apellido</th>
                <th class='th_user_list'>Tipo</th>
                <th class='th_user_list'>Estado</th>
                <th class='th_user_list'>Opciones</th>
            </tr>
        </thead>

        <tfoot class='tfoot_user_list'>
            <tr>
                <th class='th_user_list'>Usuario</th>
                <th class='th_user_list'>Nombre</th>
                <th class='th_user_list'>Apellido</th>
                <th class='th_user_list'>Tipo</th>
                <th class='th_user_list'>Estado</th>
                <th class='th_user_list'>Opciones</th>
            </tr>
        </tfoot>

        <tbody class='tbody_user_list'>
            <?php foreach ($usuarios as $user): ?>
                <tr>
                    <td class='td_user_list'><?php echo $user['usuario'] ?></td>
                    <td class='td_user_list'><?php echo $user['nombre'] ?></td>
                    <td class='td_user_list'><?php echo $user['apellido'] ?></td>
                    <td class='td_user_list'><?php echo $user['tipo'] ?></td>
                    <td class='td_user_list'><?php echo $user['estado'] ?></td>
                    <td class='td_user_list'>
                        <button class="btn btn-warning" id="<?php echo $user['id'] ?>" onclick="modify_user_tabla(this.id);">Modificar</button>
                        <button class="btn btn-danger" id="<?php echo $user['id'] ?>" onclick="delete_user(this.id)" data-toggle="modal" data-target="#myModalDelUser">Eliminar</button>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<?php $this->load->view('extras/modal_del_user'); ?>