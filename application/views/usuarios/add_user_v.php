<div class="col-ms-12 table-responsive">
    <table class='table table-striped table-hover table-bordered' id='myTableUsers'> 
        <thead class='thead_user_add'>
            <tr>
                <th class='th_user_add'>Usuario</th>
                <th class='th_user_add'>Contraseña</th>
                <th class='th_user_add'>Nombre</th>
                <th class='th_user_add'>Apellido</th>
                <th class='th_user_add'>Tipo</th>
                <th class='th_user_add'>Estado</th>
                <th class='th_user_add'>Opciones</th>
            </tr>
        </thead>

        <tbody class='tbody_user_add'>
                <tr>
                    <td class='td_user_add'>
                        <input type="text" autofocus="autofocus" required id="user_nick" name="user_nick" class="form-control" onkeypress="return validar_user_nick(event);">
                    </td>
                    <td class='td_user_add'>
                        <input type="password" required id="user_password" name="user_password" class="form-control">
                    </td>
                    <td class='td_user_add'>
                        <input type="text" required id="user_name" name="user_name" class="form-control" onkeypress="return validar_letras(event);">
                    </td>
                    <td class='td_user_add'>
                        <input type="text" required id="user_surname" name="user_surname" class="form-control" onkeypress="return validar_letras(event);">
                    </td>
                    <td class='td_user_add'>
                        <select id="user_tipo_select" name="user_tipo_select" class="form-control">
                            <option id="0" value="0">Administrador</option>
                            <option id="1" value="1">Usuario</option>
                        </select>
                    </td>
                    <td class='td_user_add'>
                        <select id="user_estado_select" name="user_estado_select" class="form-control">
                            <option id="0" value="0">Inactivo</option>
                            <option id="1" value="1">Activo</option>
                        </select>
                    </td>
                    <td class='td_user_add'>
                        <button class="btn btn-success" id="add_user_button" name="add_user_button" onclick="add_user_llamada();">Agregar</button>
                    </td>
                </tr>
        </tbody>
    </table>
</div>

<button class="btn btn-success" id="volver_a_usuarios" onclick="volver_a_usuarios();">&leftarrow; Volver</button>

<div class="col-md-4 col-md-offset-4" id="informacion" hidden>
    <div class="alert alert-danger" role="alert">
        <div id="informacion-texto"></div>
    </div>
</div>