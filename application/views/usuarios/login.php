<body id="bLogin">

    <div class="container">
        <form class="ingresoSis">

            <fieldset>

                <div class="row" id="row_login">
                    <div class="col-md-4 col-md-offset-4" id="datos_login">
                        <div class="form-group" id="grupo_login_legend">
                            <legend id="legend_login">Iniciar Sesión</legend>
                        </div>
                        <div class="form-group input-group" id="grupo_login_usuario">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input class="form-control" type="text" id="usuario" name="usuario" placeholder="Usuario" maxlength="50" />  
                        </div>
                        <div class="form-group input-group" id="grupo_login_password">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                            <input class="form-control" type="password" id="contrasena" name="contrasena" placeholder="Contraseña" maxlength="50"/>
                        </div>
                        <div class="form-group" id="grupo_login_submit">
                            <button type="submit" class="btn btn-success btn-block" id="ingresar" >Ingresar</button>
                        </div>
                    </div>
                </div>
                

            </fieldset>
            
            <div class="col-md-4 col-md-offset-4" id="informacion" hidden>
                <div class="alert alert-danger" role="alert">
                    <div id="informacion-texto">
                    </div>
                </div>
            </div>

        </form>
    </div>
    
</body>