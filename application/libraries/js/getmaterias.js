function showMaterias(id_carrera) { // Esta es la funcion principal que muestra las materias a medida que el listbox cambia de opcion.
    decision=carreras.value;
        if (id_carrera == "") {
            ("#txtHint").html(""); // Limpiar div
            document.title="Materias"; // Definir título
            return;
        } else {
            $.ajax ({
                url: "<?php echo base_url(). 'application/models/materias_model.php/listar'; ?>",
                data: { "idc" : id_carrera},
                type: 'post',
                success: function(response) {
                    $("#txtHint").html(response); // Crea la tabla y la inserta en el div
                    document.title = document.getElementById('carreras').options[document.getElementById('carreras').selectedIndex].innerHTML; // Definir título de la materia seleccionada
                }
            });
        }
}