function go_cursadas() {
    $.redirect(base_url + "cursada");
}

function go_materias() {
    $.redirect(base_url + "materia");
}

function go_usuarios() {
    $.redirect(base_url + "usuario/abm");
}

function go_cursada_user() {
    $.redirect(base_url + "cursada");
}

function go_perfil() {
    $.redirect(base_url + "cursada/perfil");
}