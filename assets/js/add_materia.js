$("#agregar_tabla").click( function(e){
    
    e.preventDefault();
    
    var carrera_actual = $("#filtroCarrera").val();
    var verificacion = 'permiso';
    
    $.ajax({ // Llamada a Ajax mas ordenada que la de anteriores versiones.
        url: base_url + "materia/add_materia",
        type: 'POST', // POST mas seguro que GET.
        data: {'ID_Carrera': carrera_actual, 'verify': verificacion}, // Parametros.
        dataType: 'html',
        success: function(response) { // Una vez que la funcion terminó (asincronía).
            document.getElementById("txtHintMat").innerHTML = response; // Lo que devuelve la función se plasma en el inner.
        }
    });
});

function add_materia(i, n, d, c, e){ // Se recopilan los datos de la pagina anterior y se mandan a la consulta de SQL.
    /*
    i = id_carrera
    n = nombre(materia)
    d = descripcion;
    c = carga_horaria;
    */

    e.preventDefault();
    
    if(n == ""){ // Validación antes de UPDATE en SQL.
        alert("El nombre de la materia no puede estar vacío")
        return;
    }
    else{
        $.ajax({ // Llamada a Ajax mas ordenada que la de anteriores versiones.
            url: base_url + "materia/add_llamada",
            type: 'POST', // POST mas seguro que GET.
            data: {'i': i, 'n': n, 'd': d, 'c': c}, // Parametros.
            dataType: 'html',
            success: function(response) { // Una vez que la funcion terminó (asincronía).
                if(response == 'OK') {
                    show_materia_by_id(i);
                    
                } else if(response == 'MATERIA_EXISTS') {
                    alert("La materia ya existe en la carrera seleccionada");
                } else {
                    console.log("Me llegó: " + response);
                }
            },
        });
    }
}