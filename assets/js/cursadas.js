// var decision; // Decision corresponde a la opcion del listbox seleccionada.

function show_usuarios() { // Esta es la funcion principal que muestra las materias a medida que el listbox cambia de opcion. 
    decision=usuarios.value;
    if(decision == 0) { // Si se quiere cargar todas las cursadas
        verificacion = 'permiso'
        get_all_cursada(verificacion);
    } else { // Si se quiere cargar las cursadas de un determinado usuario
        $.ajax({ // Llamada a Ajax mas ordenada que la de anteriores versiones. 
            url: base_url + "cursada/get_cursada_by_usuario",
            type: 'POST', //POST mas seguro que GET.
            dataType: 'text',
            data: {'Usuario': ($("#usuarios").val())}, // Parametros.
            success: function(responseText) { // Una vez que la funcion terminó (asincronía).
                document.getElementById("txtHintCur").innerHTML = responseText; // Lo que devuelve la función se plasma en el inner.
                pagination_list_cursada();
            },
        });
    }
}

$(document).ready(function(){
    var verificacion = 'permiso';
    get_all_cursada(verificacion);
});

function get_all_cursada(verificacion) {
    $.ajax({ // Llamada a Ajax mas ordenada que la de anteriores versiones. 
        url: base_url + "cursada/get_all_cursada",
        type: 'POST', //POST mas seguro que GET.
        data: { 'verify' : verificacion },
        dataType: 'text',
        success: function(responseText) { // Una vez que la funcion terminó (asincronía).
            document.getElementById("txtHintCur").innerHTML = responseText; // Lo que devuelve la función se plasma en el inner.
            pagination_list_cursada();
        },
    });
}

function add_cursada(){ // Se pasa a la pagina donde se agregan las cursadas.
    var user_act=$("#usuarios").val();
    var verificacion='permiso';
    $.ajax({ // Llamada a Ajax mas ordenada que la de anteriores versiones.
        url: base_url + "cursada/add_cursada",
        type: 'POST', //POST mas seguro que GET.
        data: {'user_act': user_act, 'verify': verificacion},
        success: function(responseText) { // Una vez que la funcion terminó (asincronía).
            document.getElementById("txtHintCur").innerHTML = responseText; // Lo que devuelve la función se plasma en el inner.
        },
    });
}

function add_cursada_llamada(i, m, n){ // Se recopilan los datos de la pagina anterior y se mandan a la consulta de SQL.
    /*
    i = id_usuario
    m = id_materia
    n = nota
    */
    if(i == "" || m == "" || n == "" ){ // Validación antes de UPDATE en SQL.
        alert("No pueden existir campos en blancos.")
        return;
    } else if(n < 0 || n > 10) {
        alert("La nota debe ser entre 0 y 10");
        return;
    } else{
        $.ajax({ // Llamada a Ajax mas ordenada que la de anteriores versiones.
            url: base_url + "/cursada/add_llamada",
            type: 'POST', //POST mas seguro que GET.
            data: {'i': i, 'm': m, 'n': n}, // Parametros.
            success: function(responseText) { // Una vez que la funcion terminó (asincronía).
                document.getElementById("txtHintCur").innerHTML = responseText; // Lo que devuelve la función se plasma en el inner.
                document.getElementById("usuarios").value = i; // Para que cuando agregue la cursada, me lleve a ver las demás cursadas del usuario.
                show_usuarios(); // El i, esta haciendo referencia al id de usuario agregado.
            },
        });
    }
}

function delete_cursada(ID_Cursada) { // Esta funcion elimina la cursada seleccionada.
    $("#del_cursada_confirm").click(function(){
        $.ajax({ // Llamada a Ajax mas ordenada que la de anteriores versiones.
            url: base_url + "cursada/delete_cursada",
            type: 'POST', //POST mas seguro que GET.
            data: {'ID_Cursada': ID_Cursada}, // Parametros.
            success: function(responseText) { // Una vez que la funcion terminó (asincronía).
                document.getElementById("txtHintCur").innerHTML = responseText; // Lo que devuelve la función se plasma en el inner.
                show_usuarios(); // Esto es para que cuando se termine de actualizar vuelva la tabla.
                $('.modal-backdrop').remove();
            },
        });
    });
}

function modify_cursada(ID_Cursada){ //Esta es la primera parte de la funcion modificar, exhibe por pantalla la cursada a modificar y permite modificar.
    var valorlistboxusuarios = document.getElementById("usuarios"); //Esto lo necesito para no hacer otro JOIN mas adelante.
    var Alumno = valorlistboxusuarios.options[valorlistboxusuarios.selectedIndex].text; //Esto lo necesito para no hacer otro JOIN mas adelante.
    modify_cursada_by_alumno(ID_Cursada, Alumno);
}

function modify_cursada_by_alumno(ID_Cursada, Alumno) {
    $.ajax({ // Llamada a Ajax mas ordenada que la de anteriores versiones.
        url: base_url + "cursada/modify_cursada",
        type: 'POST', //POST mas seguro que GET.
        data: {'ID_Cursada': ID_Cursada, 'Alumno': Alumno }, // Parametros.
        success: function(responseText) { // Una vez que la funcion terminó (asincronía).
            document.getElementById("txtHintCur").innerHTML = responseText; // Lo que devuelve la función se plasma en el inner.
        },
    });
}
    
function aceptar_cursada(id_cursada){ // Esta funcion recibe los datos de la anterior y manda la consulta con los datos actualizados.
    var cur_user = $('#cur_user_select').val();
    var cur_mat = $('#mat_user_select').val();
    var cur_nota = $('#nota_user').val();
    
    if(cur_nota == ""){ // Validación antes de UPDATE en SQL.
        alert("La nota de la cursada no puede estar vacia")
        return;
    } else if(cur_nota < 0 || cur_nota > 10) {
        alert("La nota debe estar entre 0 y 10");
        return;
    } else{
        $.ajax({ // Llamada a Ajax mas ordenada que la de anteriores versiones.
            url: base_url + "cursada/modify_llamada",
            type: 'POST', //POST mas seguro que GET.
            data: {'id_cursada': id_cursada, 'cur_user': cur_user, 'cur_mat': cur_mat, 'cur_nota': cur_nota}, // Parametros.
            success: function(responseText) { // Una vez que la funcion terminó (asincronía).
                document.getElementById("txtHintCur").innerHTML = responseText; // Lo que devuelve la función se plasma en el inner.
                document.getElementById("usuarios").value = cur_user; // Al modificar le recuerdo al usuario cual era el usuario.
                show_usuarios(); // Esto es para que cuando se termine de actualizar vuelva la tabla.
            },
        });
    }

}