/*$("#login_boton").click(function(){
    var Usuario = document.getElementById("uname").value;
    var Contra = document.getElementById("psw").value;
    if(Usuario == "" || Contra == ""){
        alert("Los campos de inicio de sesión no pueden estar vacios");
        return;
    }
    $.ajax({ // Llamada a Ajax mas ordenada que la de anteriores versiones.
        url: base_url + "usuario/login",
        type: 'POST', //POST mas seguro que GET.
        data: {'Usuario': Usuario, 'Contra': Contra}, // Parametros.
        success: function(responseText) { // Una vez que la funcion terminó (asincronía).

            if(!(responseText=="Administrador") && !(parseInt(responseText)>0)){
                alert("Usuario y/o contraseña no válidos, asegurese de estar activo");
            }

            if(responseText=="Administrador"){
                 $.redirect(base_url + "cursada/index_admin", "POST");
            }

            if(parseInt(responseText)>0){ //Si no es administrador me importa su ID para realizar la busqueda de Cursadas.
                $.redirect(base_url + "cursada/index",{usuario_id: responseText}, "POST");    
            }

        },

    });
});
*/

$("#ingresar").click(function(e){ 
    
    e.preventDefault();
    
    $.ajax({
         type: "POST",
         url: base_url + "usuario/validarIngreso", 
         data: { usuario: $("#usuario").val(), contrasena: $("#contrasena").val() },
         dataType: "text",  
         cache:false,
         success: 
              function(data){
                $("#informacion-texto").empty();
                switch(data){
                    case "OK":  // redirecciono al home
                            window.location = base_url + "cursada";
                            break;
                    
                    case "ERROR_ESTADO":
                            $("#informacion-texto").html("<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> El usuario se encuentra inactivo");
                            $("#informacion").show();
                            break;
                            
                    case "ERROR_INCORRECTO":
                            $("#informacion-texto").html("<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> Hay datos vacíos o incorrectos");   
                            $("#informacion").show();
                            break;
                    
                    case "ERROR_NO_EXISTE":
                            $("#informacion-texto").html("<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> El usuario no existe");
                            $("#informacion").show();
                            break;
                            
                    case "ERROR_CONTRASENA":
                            $("#informacion-texto").html("<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> La contraseña es incorrecta");    
                            $("#informacion").show();
                            break;
                            
                    default:
                            $("#informacion-texto").html("<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> Estoy en el default, no debería pasar, me llegó:" + data);
                            $("#informacion").show();
                            break;
                }
              }
    });
});