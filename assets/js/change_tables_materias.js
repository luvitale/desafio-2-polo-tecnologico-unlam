$('#filtroCarrera').on('change', function(){ // Cuando se cambia a otra carrera
    if($('#filtroCarrera').val() == 0) { // Si se selecciona todas
        show_all_materia(); // Mostrar todas las materias
    } else { // Si se selecciona una carrera en particular
        show_materia_by_id($('#filtroCarrera').val()); // Mostrar las materias correspondientes
    }
});

$(document).ready(function(){
    var carrera_selec = $('#filtroCarrera').val();
    var verificacion = 'permiso';
    $.ajax({ // Llamada a Ajax mas ordenada que la de anteriores versiones. 
        url: base_url + "materia/list_materia/" + carrera_selec,
        type: 'POST', // POST mas seguro que GET.
        dataType: 'html',
        data: {'ID_Carrera': carrera_selec, 'verify': verificacion}, // Parametros.
        success: function(response) { // Una vez que la funcion terminó (asincronía).
            document.getElementById("txtHintMat").innerHTML = response; // Lo que devuelve la función se plasma en el inner.
            pagination_list_materia();
        },
    });
});

function pagination_list_materia() { // Carga la paginación de la tabla del listado de materias
    $('#myTableListMateria').DataTable( {
        "drawCallback": function( settings ) {
            if(!$("#myTableListMateria").parent().hasClass("table-responsive")){
                $("#myTableListMateria").wrap("<div class='table-responsive'></div>");
            }
        },
        "language": {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
        }
    });
}

function show_all_materia() {
    window.location=0;
    
}

function show_materia_by_id(id_carrera) {
    window.location=id_carrera;
}
