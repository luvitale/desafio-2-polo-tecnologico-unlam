function enviar_mail() {
    var datos = cargar_datos_cursada_email();
    $("#enviar_mail").addClass("disabled");
    $("#enviar_mail").text("Enviando...");
    $.ajax({ // Llamada a Ajax mas ordenada que la de anteriores versiones. 
            url: base_url + "cursada/enviar_mail",
            type: 'POST', //POST mas seguro que GET.
            dataType: 'text',
            data: {'mail': ($("#mail").val()), 'datos': datos}, // Parametros.
            success: function(data) { // Una vez que la funcion terminó (asincronía).
                switch(data) {
                    case "OK":
                        $("#mail").val("");
                        alert('¡Correo Enviado!');
                        break;
                    case "EMPTY":
                        alert('El campo está vacío. Ingrese un mensaje');
                        break;
                    default:
                        console.log(data);
                        break;
                }
                $("#enviar_mail").removeClass("disabled");
                $("#enviar_mail").text("Enviar Correo con Cursadas");
            },
        });
}

function cargar_datos_cursada_email() {
    var datos = "<!doctype html>\n<html>\n\
                    <head>\n\
                        <link href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' rel='stylesheet' integrity='sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u' crossorigin='anonymous'>\n\
                    </head>\n";
    datos += '<div class="col-ms-12">\n\<table class="table table-bordered">\n\
                <caption style="text-align:center;font-weight:bold;color:black;width:100%;">' + document.title + '</caption>\n\
                <thead>\n\
                    <tr>\n\
                        <th style="text-align:center;">    Materia </th>\n\
                        <th style="text-align:center;">    Nota    </th>\n\
                    </tr>\n\
                </thead>';
    datos +=   '<tbody>\n';
    $("td#nombre_mat_alum").each(function(index) {
        datos +=    '<tr>\n';
        datos +=        "<td style='text-align:center;'> "    +   $(this).text()                          +   " </td>\n";
        datos +=        "<td style='text-align:center;'> "    +   $("td#nota_mat_alum" + index).text()    +   " </td>\n";
        datos +=    '</tr>\n';
    });
    datos +=        '</tbody>\n\
               </table>\n\</div>\n';
    datos += "</body>\n</html>";
    return datos;
}

function enviar_mensaje() {
    $("#enviar_mensaje").addClass("disabled");
    $("#enviar_mensaje").text("Enviando...");
    $.ajax({ // Llamada a Ajax mas ordenada que la de anteriores versiones. 
        url: base_url + "cursada/enviar_mensaje_alumno",
        type: 'POST', //POST mas seguro que GET.
        dataType: 'text',
        data: {'mensaje': ($("#mensaje").val()), 'usuario_id': usuario_id}, // Parametros.
        success: function(data) { // Una vez que la funcion terminó (asincronía).
            switch(data) {
                case "OK":
                    $("#mensaje").val("");
                    alert('¡Mensaje Enviado!');
                    break;
                case "EMPTY_MSG":
                    alert('El campo está vacío. Ingrese un mensaje');
                    break;
                default:
                    console.log(data);
                    break;
            }
            $("#enviar_mensaje").removeClass("disabled");
            $("#enviar_mensaje").text("Enviar Mensaje a los Administradores");
        },
    });
}