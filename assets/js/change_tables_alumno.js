$(document).ready(function(){
    pagination_list_user_cursada();
});

function pagination_list_user_cursada() { // Carga la paginación de la tabla del listado de materias
    $('#myTableListUserCursada').DataTable( {
        "drawCallback": function( settings ) {
            if(!$("#myTableListUserCursada").parent().hasClass("table-responsive")){
                $("#myTableListUserCursada").wrap("<div class='table-responsive'></div>");
            }
        },
        "language": {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
        }
    });
}