function delete_materia(ID_Materia, ID_Carrera, e) { // Esta funcion elimina la materia seleccionada.
    
    e.preventDefault();
    
    $("#del_materia_confirm").click(function(){
        $.ajax({
            url: base_url + "materia/delete_materia",
            type: 'POST', //POST mas seguro que GET.
            data: {'ID_Materia': ID_Materia}, // Parametros.
            dataType: 'html',
            success: function(response) { // Una vez que la funcion terminó (asincronía).
                show_materia_by_id(ID_Carrera);
            },
        });
    });
        
}

function modify_materia(ID_Materia, e){ //Esta es la primera parte de la funcion modificar, exhibe por pantalla la materia a modificar y permite modificar.

    e.preventDefault();    
    
    $.ajax({ // Llamada a Ajax mas ordenada que la de anteriores versiones.
        url: base_url + "materia/modify_materia",
        type: 'POST', //POST mas seguro que GET.
        data: {'ID_Materia': ID_Materia}, // Parametros.
        dataType: 'html',
        success: function(response) { // Una vez que la funcion terminó (asincronía).
            document.getElementById("txtHintMat").innerHTML = response; // Lo que devuelve la función se plasma en el inner.
        },
    });
}

function aceptar_modify_materia(id, i, n, d, c, e){ // Esta funcion recibe los datos de la anterior y manda la consulta con los datos actualizados.
    /*
    id = id_materia
    i = id_carrera
    n = nombre(materia)
    d = descripcion;
    c = carga_horaria;
    */
   
    e.preventDefault();
   
    if(n == ""){ // Validación antes de UPDATE en SQL.
        alert("El nombre de la materia no puede estar vacío");
        return;
    }
    
    else{
        $.ajax({ // Llamada a Ajax mas ordenada que la de anteriores versiones.
            url: base_url + "materia/modify_llamada",
            type: 'POST', //POST mas seguro que GET.
            data: {'id': id, 'i': i, 'n': n, 'd': d, 'c': c}, // Parametros.
            dataType: 'html',
            success: function(response) { // Una vez que la funcion terminó (asincronía).
                if(response == 'OK') {
                    show_materia_by_id(i);
                    
                } else if(response == 'MATERIA_EXISTS') {
                    alert('El nombre de materia ingresado ya existe en la carrera seleccionada');
                    
                } else {
                    console.log("Me llegó: " + response);
                    
                }
            },
        });
    }

}