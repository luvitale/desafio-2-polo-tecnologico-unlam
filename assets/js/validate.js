function validar_numeros(event){ // La validación funciona en IE, Firefox, Chrome y además permite acentos, diéresis y enie.
    var charcode = event.charCode;
    
    // valido que sea solo letra lo que ingresa
    if(charcode == 0){ // Firefox Backspace charcode 0.
        return;
    }
    if (charcode < num_cero || charcode > num_nueve){
        return false;
    }
}

function validar_letras(event){ // La validación funciona en IE, Firefox, Chrome y además permite acentos, diéresis y eñe.
    var charcode = event.charCode;
    
    // valido que sea solo letra lo que ingresa
     
    if(charcode == null_caracter){ // Firefox Backspace charcode 0.
        return;
    }
    if (charcode > space && (charcode < interrog_cierre || charcode > z_mayus) && (charcode < a_minus || charcode > z_minus ) && (charcode < interrog_apertura || charcode > u_con_dieresis) && (charcode < comilla || charcode > punto)){
        return false;
    }
}

function validar_letras_materia(event){ // La validación funciona en IE, Firefox, Chrome y además permite acentos, diéresis y eñe.
    var charcode = event.charCode;
    
    // valido que sea solo letra lo que ingresa
     
    if(charcode == null_caracter){ // Firefox Backspace charcode 0.
        return;
    }
    if (charcode > space && (charcode < interrog_cierre || charcode > z_mayus) && (charcode < a_minus || charcode > z_minus ) && (charcode < interrog_apertura || charcode > u_con_dieresis) && (charcode < comilla || charcode > punto) && (charcode < num_cero || charcode > num_nueve)){
        return false;
    }
}

function validar_user_nick(event) {
    var charcode = event.charCode;
    
    if(charcode == null_caracter){ // Firefox Backspace charcode 0.
        return;
    }
    if (charcode >= space && (charcode < a_mayus || charcode > z_mayus) && (charcode < a_minus || charcode > z_minus ) && charcode != guion_bajo ){
        return false;
    }
}