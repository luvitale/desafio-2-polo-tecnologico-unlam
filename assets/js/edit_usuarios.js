$(document).ready(function(){
    show_users('permiso');
});

function show_users(verificacion) {
    $.ajax({
        url: base_url + "usuario/show_user",
        type: 'POST', // POST mas seguro que GET.
        data: {'verify': verificacion},
        dataType: 'html',
        success: function(response) { // Una vez que la funcion terminó (asincronía).
            $("#txtHintUser").html(response); // Se agrega la tabla para realizar la acción
            pagination_users_abm();
        },
    });
}

$("#cargar_tabla_add_user").click(function(){
    var verificacion='permiso';
    $.ajax({
        url: base_url + "usuario/add_user",
        type: 'POST', // POST mas seguro que GET.
        data: {'verify': verificacion},
        dataType: 'html',
        success: function(response) { // Una vez que la funcion terminó (asincronía).
            $("#txtHintUser").html(response); // Se agrega la tabla para realizar la acción
        },
    });
});

function add_user_llamada() {
    var verificacion = 'permiso';
    var user_nick = $('#user_nick').val();
    var user_password = $('#user_password').val();
    var user_name = $('#user_name').val();
    var user_surname = $('#user_surname').val();
    $.ajax({
        url: base_url + "usuario/validar_add_user",
        type: 'POST', // POST mas seguro que GET.
        data: {'user_nick': user_nick, 'user_password': user_password, 'user_name': user_name, 'user_surname': user_surname, 'user_tipo': $('#user_tipo_select').val(), 'user_estado': $('#user_estado_select').val()},
        dataType: 'text',
        cache: 'false',
        success: function(data) { // Una vez que la funcion terminó (asincronía).
            switch(data) {
                
                case 'OK':  // Muestro los usuarios
                    show_users(verificacion);
                    break;
                    
                case 'ERROR_INCORRECTO':
                    $("#informacion-texto").html("<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> Hay datos vacíos o incorrectos");   
                    $("#informacion").show();
                    break;
                    
                case 'USER_EXISTS':
                    $("#informacion-texto").html("<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> El usuario ya existe");   
                    $("#informacion").show();
                    break;
                    
                default:
                    $("#informacion-texto").html("<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> Estoy en el default, no debería pasar, me llegó:" + data);
                    $("#informacion").show();
                    break;
                    
            }
        },
    });
}

function modify_user_tabla(id_usuario){ //Esta es la primera parte de la funcion modificar, exhibe por pantalla el usuario a modificar
    $.ajax({
        url: base_url + "usuario/modify_user",
        type: 'POST', // POST mas seguro que GET.
        data: {'user_id': id_usuario}, // Parametros.
        dataType: 'html',
        success: function(response) { // Una vez que la funcion terminó (asincronía).
            $("#txtHintUser").html(response); // Obtengo la tabla para realizar esta tarea
        },
    });
}

function modify_user_llamada(id_usuario) {
    var user_nick = $('#user_nick').val();
    var user_password = $('#user_password').val();
    var user_name = $('#user_name').val();
    var user_surname = $('#user_surname').val();
    $.ajax({
        url: base_url + "usuario/validar_modify_user",
        type: 'POST', // POST mas seguro que GET.
        data: {'user_id': id_usuario, 'user_nick': user_nick, 'user_password': user_password, 'user_name': user_name, 'user_surname': user_surname, 'user_tipo': $('#user_tipo_select').val(), 'user_estado': $('#user_estado_select').val()},
        dataType: 'text',
        cache: 'false',
        success: function(data) { // Una vez que la funcion terminó (asincronía).
            if(user_nick == '') {
                $('input#user_nick').addClass('has-error');
            } else {
                $('#user_nick').removeClass('has_error');
            }
            if(user_password == '') {
                $('#user_password').addClass('has-error');
            } else {
                $('#user_password').removeClass('has_error');
            }
            if(user_name == '') {
                $('#user_name').addClass('has-error');
            } else {
                $('#user_name').removeClass('has_error');
            }
            if(user_surname == '') {
                $('#user_surname').addClass('has-error');
            } else {
                $('#user_surname').removeClass('has_error');
            }
            switch(data) {
                case 'OK':  // Muestro los usuarios
                    show_users('permiso');
                    break;
                    
                case 'ERROR_INCORRECTO':
                    console.log(data);
                    $("#informacion-texto").html("<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> Hay datos vacíos o incorrectos");   
                    $("#informacion").show();
                    break;
                    
                case 'USER_EXISTS':
                    $("#informacion-texto").html("<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> El usuario ya existe");   
                    $("#informacion").show();
                    break;
                    
                default:
                    $("#informacion-texto").html("<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> Estoy en el default, no debería pasar, me llegó:" + data);
                    $("#informacion").show();
                    break;
                    
            }
        },
    });
}

function delete_user(user_id) {
    $("#del_user_confirm").click(function(){
        $.ajax({ // Llamada a Ajax mas ordenada que la de anteriores versiones.
            url: base_url + "usuario/delete_user",
            type: 'POST', //POST mas seguro que GET.
            data: {'user_id': user_id}, // Parametros.
            success: function(responseText) { // Una vez que la funcion terminó (asincronía).
                show_users('permiso');
                $('.modal-backdrop').remove();
            },
        });
    });
}

function volver_a_usuarios() {
    show_users('permiso');
}