-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-04-2017 a las 03:22:56
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `desafio`
--
CREATE DATABASE IF NOT EXISTS `desafio` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `desafio`;

--
-- Volcado de datos para la tabla `carreras`
--

INSERT INTO `carreras` (`id`, `nombre`, `descripcion`) VALUES
(1, 'Arquitectura', 'Es la descripcion de Arquitectura'),
(2, 'Ingenieria Mecanica', 'Es la descripcion de Ingenieria Mecanica'),
(3, 'Ingenieria en Informatica', 'Es la descripcion de Ingenieria en Informatica'),
(4, 'Ingenieria en Electronica', 'Es la descripcion de InIngenieria en Electronica'),
(5, 'Ingenieria Industrial', 'Es la descripcion de Ingenieria Industrial'),
(6, 'Ingenieria Civil', 'Es la descripcion de Ingenieria Civil'),
(7, 'Desarrollo Web', 'Es la descripcion de Desarrollo Web'),
(8, 'Sonido y Grabacion', 'Es la descripcion de Sonido y Grabacion');

--
-- Volcado de datos para la tabla `cursadas`
--

INSERT INTO `cursadas` (`id`, `usuario_id`, `materia_id`, `nota`, `fecha`) VALUES
(1, 3, 4, 7, '2017-03-31 16:00:14'),
(4, 5, 8, 9, '2017-03-31 18:31:15'),
(5, 5, 9, 10, '2017-03-31 18:31:15'),
(8, 3, 5, 8, '2017-04-03 19:43:22'),
(11, 3, 6, 5, '2017-04-03 20:49:41');

--
-- Volcado de datos para la tabla `materias`
--

INSERT INTO `materias` (`id`, `carrera_id`, `nombre`, `descripcion`, `carga_horaria`) VALUES
(1, 1, 'Dibujo Tecnico', 'Autocad, Laminas', 4),
(2, 1, 'Construcciones', 'Modelaje', 8),
(4, 2, 'Maquinarias', 'Trabajos Practicos', 4),
(5, 2, 'Circuitos', 'Electricidad', 4),
(6, 2, 'Contaminacion', 'Ambiente', 4),
(8, 3, 'Base de Datos', 'Organizacion de Datos', 6),
(9, 3, 'Sistemas Operativos', 'Teoria', 8),
(10, 4, 'Circuitos Electricos', 'Teoria', 6),
(11, 4, 'Programacion Robotica', 'Futuro', 10),
(12, 4, 'Nanotecnologia', 'Tecnologia Avanzada', 8),
(13, 5, 'Control Ambiental', 'Empresas', 4),
(14, 5, 'Control de Calidad', 'Procesos', 4),
(15, 5, 'Maquinaria Pesada', 'Teoria', 8),
(16, 6, 'Planos y Construcciones', 'Diseño', 8),
(17, 6, 'Herramientas de diseño', 'Tecnologia', 4),
(18, 6, 'Dibujo Avanzado', 'Detalles', 4),
(19, 7, 'HTML', 'Sin agregados', 4),
(20, 7, 'PHP, CSS, JQUERY', 'Aplicaciones', 8),
(21, 7, 'SQL', 'Base de Datos', 4),
(22, 8, 'Procesos de Grabacion', 'Historia', 4),
(23, 8, 'Equipos de Grabacion', 'Tecnico', 4),
(30, 8, 'Sonido Intermedio', 'Optativo', 4),
(31, 3, 'Proyecto Extraprogramatico', 'Optativo', 6),
(32, 1, 'Diseños Automáticos', 'Requerido', 6);

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `nombre`, `apellido`, `tipo`, `estado`, `fechaAlta`, `contrasena`) VALUES
(1, 'Juan_Perez', 'Juan', 'Perez', 0, 1, '2017-04-13 08:35:30', 'a176128aefc953e3fac56a6cfbc0ed5089e45e44'),
(2, 'Sergio_Rodriguez', 'Sergio', 'Rodriguez', 0, 0, '2017-04-13 08:46:06', '637ff14a8a8020f0f39ba2dd58d312948aad582e'),
(3, 'Rodrigo_Escobar', 'Rodrigo', 'Escobar', 1, 1, '2017-04-13 08:46:25', '7c4a8d09ca3762af61e59520943dc26494f8941b'),
(4, 'Matias_Ramirez', 'Matias', 'Ramirez', 1, 0, '2017-04-13 08:46:43', 'dd5fef9c1c1da1394d6d34b248c51be2ad740840'),
(5, 'Pedro_Enrique', 'Pedro', 'Enrique', 1, 1, '2017-04-13 08:46:59', '3dd0383c55c8d7cdc0830c0634c7791a8061a4d1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
