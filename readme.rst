###################
DesafioMVC versi�n WEB
###################

El proyecto DesafioMVC tiene como objetivo realizar un sistema web ABM(Alta, Baja, Modificacion) con relaci�n a los alumnos, cursadas y materias de una universidad. Las siglas MVC significa Modelo-Vista-Controlador, el cual es un dise�o de software que se basa en dividir en 3 capas, las mencionadas anteriormente, para as� tener una programaci�n mas organizada y legible.

*******************
Framework del Proyecto

*******************
El proyecto fue desarrollado utilizando CodeIgniter un framework de desarrollo el cual se puede acoplar a un programa de entorno de desarrollo como Netbeans, para hacer mas intuitivo la programaci�n.
Al ser un desarrollo web se utiliza HTML, CSS, PHP, JS.

************
Instalaci�n y Prueba del Proyecto
************

Para probar el proyecto se debe tener instalado el XAMPP, con los modulos Apache y MYSQL activos. Se deber� crear una carpeta en el directorio C:\\xampp\\htdocs llamada DesafioMVC, quedando el directorio C:\\xampp\\htdocs\\DesafioMVC. Una vez realizado esto se debe importar la base de datos indicada en DesafioMVC\\bd mediante el Phpmyadmin. Finalmente mediante cualquier navegador se deber� acceder a http://localhost/DesafioMVC donde se encontrar� la pantalla de Login de Usuarios.
*En el proyecto se encuentra un archivo Probar Usuarios.txt el cual se especifican los usuarios creados y los privilegios de cada uno.

*******
Finalidad del proyecto
*******

El proyecto tiene como finalidad la realizaci�n de un sistema de altas, bajas, modificaciones y listado de las entidades de una universidad(alumno, materias y cursadas). Adem�s de contar con un sistema de autenticac�on de usuarios(alumno/profesor) los cuales tienen distintos accesos y atributos. La parte est�tica no tuvo demasiada relevancia, dandol� prioridad al funcionamiento �ptimo del sistema.

*********
Archivos Externos
*********

Los archivos externos como los .JS y .CSS se encuentran alojados en un servidor de Dropbox, esto permite no tener inconvenientes con los archivos locales, pero en algunas ocasiones disminuyen el rendimiento.

***************
Contacto
***************

Para realizar alg�n contacto con respecto al proyecto:
Reflejo: tocinonaro_juan@yahoo.com.ar